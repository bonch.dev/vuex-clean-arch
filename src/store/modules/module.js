import axios from 'axios';

const moduleName = 'module';

// Не работает подсказчик
export const getModuleNames = (moduleName) => `${moduleName}/${moduleNames[moduleName]}`;

// Не работает дефолтное имя модуля
const moduleNames = { 
    LOAD_MODULE_REQUEST: 'LOAD_MODULE_REQUEST',
    LOAD_FAILED: 'LOAD_FAILED',
    LOAD_MODULE_REQUEST_SUCCESS: 'LOAD_MODULE_REQUEST_SUCCESS',

    ANOTHER_LOAD_MODULE_REQUEST: 'ANOTHER_LOAD_MODULE_REQUEST',
    ANOTHER_LOAD_MODULE_REQUEST_SUCCESS: 'ANOTHER_LOAD_MODULE_REQUEST_SUCCESS'
}

const state = {
    name: '',
    dataArray: [],
    status: ''
};

const getters = {
    name: state => state.name,
    data: state => state.dataArray,
    filteredData: state => filterState => state.data.filter(data => data.state === filterState)
};

const actions = {
    [moduleNames.LOAD_MODULE_REQUEST]: ({ commit, dispatch }) => {
        commit(moduleNames.LOAD_MODULE_REQUEST)
        return new Promise((resolve, reject) => {
            axios.get('/api/module')
                .then(resp => {
                    commit(moduleNames.LOAD_MODULE_REQUEST_SUCCESS, resp.data);
                    dispatch(moduleNames.ANOTHER_LOAD_MODULE_REQUEST);
                    resolve(resp);
                })
                .catch(resp => {
                    reject(resp);
                    commit(moduleNames.LOAD_FAILED)
                })
        })
    },
    [moduleNames.ANOTHER_LOAD_MODULE_REQUEST]: ({ commit/*, dispatch */ }) => {
        return new Promise((resolve, reject) => {
            axios.post('/api/another/module')
                .then(resp => {
                    commit(moduleNames.ANOTHER_LOAD_MODULE_REQUEST_SUCCESS, resp.data);
                    resolve(resp)
                })
                .catch(resp => {
                    reject(resp)
                    commit(moduleNames.LOAD_FAILED)
                })
        })
    }
};

const mutations = {
    [moduleNames.LOAD_MODULE_REQUEST]: (state, data) => {
        state.status = 'loading'
    },
    [moduleNames.LOAD_MODULE_REQUEST_SUCCESS]: (state, data) => {
        state.dataArray = data; // Иногда нужен вариант с Object.assign()
    },
    [moduleNames.ANOTHER_LOAD_MODULE_REQUEST_SUCCESS]: (state, data) => {
        state.dataArray = data;
        state.status = 'success'
    },
    [moduleNames.LOAD_FAILED]: (state) => {
        state.status = 'error'
    }
};

export default {
    namespaced: true,
    actions,
    state,
    getters,
    mutations
}