import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
    name: '',
    dataArray: [],
    anotherStatus: 'anotherStatus'
};

export default {
    actions,
    state,
    getters,
    mutations
}