import axios from "axios";

import {
    ANOTHER_LOAD_HEAVY_MODULE_REQUEST,
    ANOTHER_LOAD_HEAVY_MODULE_REQUEST_ERROR,
    ANOTHER_LOAD_HEAVY_MODULE_REQUEST_SUCCESS,

    LOAD_HEAVY_MODULE_REQUEST,
    LOAD_HEAVY_MODULE_REQUEST_ERROR,
    LOAD_HEAVY_MODULE_REQUEST_SUCCESS
} from "./names";

const loadRequest = ({commit, dispatch }) => {
    return new Promise((resolve, reject) => {
        commit(LOAD_HEAVY_MODULE_REQUEST);
        axios.get('/api/HEAVY_MODULE')
            .then((resp) => {
                commit(LOAD_HEAVY_MODULE_REQUEST_SUCCESS, resp.data);
                dispatch(ANOTHER_LOAD_HEAVY_MODULE_REQUEST);
                resolve(resp);
            })
            .catch((resp) => {
                commit(LOAD_HEAVY_MODULE_REQUEST_ERROR);
                reject(resp);
            })
    })
};

const anotherLoadRequest = ({commit, /* dispatch */}) => {
    return new Promise((resolve, reject) => {
        commit(ANOTHER_LOAD_HEAVY_MODULE_REQUEST);
        axios.post('/api/HEAVY_MODULE')
            .then((resp) => {
                commit(ANOTHER_LOAD_HEAVY_MODULE_REQUEST_SUCCESS, resp.data);
                resolve(resp)
            })
            .catch((resp) => {
                commit(ANOTHER_LOAD_HEAVY_MODULE_REQUEST_ERROR);
                reject(resp)
            })
    })
};

export default {
    [LOAD_HEAVY_MODULE_REQUEST]: loadRequest,
    [ANOTHER_LOAD_HEAVY_MODULE_REQUEST]: anotherLoadRequest
};