const name = (state) => state.name;

const data = (state) => state.dataArray;

export default {
    name,
    data
};