import {
    LOAD_HEAVY_MODULE_REQUEST,
    LOAD_HEAVY_MODULE_REQUEST_ERROR,
    LOAD_HEAVY_MODULE_REQUEST_SUCCESS
} from "./names";

const loadMutation = (state) => {
    state.status = 'loading'
};

const successMutation = (state, data) => {
    state.dataArray = data; // Иногда нужен вариант с Object.assign()
    state.status = 'success'
};

const errorMutation = (state) => {
    state.status = 'error'
};

export default {
    [LOAD_HEAVY_MODULE_REQUEST]: loadMutation,
    [LOAD_HEAVY_MODULE_REQUEST_SUCCESS]: successMutation,
    [LOAD_HEAVY_MODULE_REQUEST_ERROR]: errorMutation,
};