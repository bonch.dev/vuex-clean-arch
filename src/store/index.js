import Vue from 'vue'
import Vuex from 'vuex'

import module from '~/store/modules/module'
import heavyModule from '~/store/modules/heavy_module'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        module,
        heavyModule
    },
    strict: debug,
})